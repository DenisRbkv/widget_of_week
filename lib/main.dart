import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Animation Widgets';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: Center(
          child: MyStatefulWidget(),
        ),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);

  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  double targetValue = 100.0;
  bool _visible = true;
  int _count = 0;


  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        AnimatedOpacity(
          // If the widget is visible, animate to 0.0 (invisible).
          // If the widget is hidden, animate to 1.0 (fully visible).
          opacity: _visible ? 1.0 : 0.0,
          duration: Duration(milliseconds: 500),
          // The green box must be a child of the AnimatedOpacity widget.
          child: TweenAnimationBuilder(
            tween: Tween<double>(begin: targetValue, end: targetValue),
            duration: Duration(seconds: 2),
            builder: (BuildContext context, double size, Widget child) {
              return AnimatedSwitcher(
                duration: const Duration(milliseconds: 500),
                transitionBuilder: (Widget child, Animation<double> animation) {
                  return RotationTransition(child: child, turns: animation);
                },
                child: Container(
                  key: ValueKey<int>(_count),
                  width: size,
                  height: size,
                  decoration: BoxDecoration(
                    color: Colors.deepOrange,
                    borderRadius: BorderRadius.circular(10),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black38,
                        blurRadius: 10.0,
                        spreadRadius: 0.0,
                        offset: Offset(
                          4.0, // horizontal
                          3.0, // vertical
                        ),
                      ),
                    ],
                  ),
                ),
              );
            },
          ),
        ),
        SizedBox(height: 50.0),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            RaisedButton(
              child: Text('Tween Size'),
              onPressed: () {
                setState(() {
                  targetValue = targetValue == 100.0 ? 200.0 : 100.0;
                  if (_visible != true) {
                    _visible = !_visible;
                  }
                });
              },
            ),
            RaisedButton(
              child: Text('Opacity'),
              onPressed: () {
                setState(() {
                  _visible = !_visible;
                });
              },
            ),
            RaisedButton(
              child: Text('Flip'),
              onPressed: () {
                setState(() {
                  if (_visible != true) {
                    _visible = !_visible;
                  }
                  _count++;
                });
              },
            ),
          ],
        ),
      ],
    );
  }
}
